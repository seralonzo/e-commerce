<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>template</title>
	<!-- Boots Watch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark position-sticky">
		 	<a class="navbar-brand" href="#">Ricurry</a>
		  	<button class="navbar-toggler" type="button" data-toggle=		"collapse" data-target="#navbarColor02"
		  			 aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor02">
		    	<ul class="navbar-nav mr-auto">
		      		<li class="nav-item active">
		        		<a class="nav-link" href="catalog.php">Lifestyle <span class="sr-only">(current)</span></a>
		            </li>
		      		<li class="nav-item">
		        		<a class="nav-link" href="add-Item.php">Add Items</a>
		            </li>
		      		<li class="nav-item">
		        		<a class="nav-link" href="cart.php">Cart</a>
		      		</li>
		      		<li class="nav-item">
		        		<a class="nav-link" href="#">About</a>
		      		</li>
		    	</ul>
		   </div>
		</nav>
		<!-- Page Contents -->

		<?php
			get_body_contents()
		?>


		<!-- Footer -->
		<footer class="page-footer bg-dark font-small navbar-dark">
			<div class="footer-copyright text-center py-3"> 2020 made for you</div>
		</footer>

</body>
</html>